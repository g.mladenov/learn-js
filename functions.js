// Functions and Scope
// Function - a function is a block of organized, reusable code that is used to perform a single, related action.
// It can also be described as a set of statements that perform a task or calculate a value. To use a function you
// must first define it.

// A function declaration consists of the 'function' keyword, followed by the name of the function, a list of optional
// parameters enclosed in parentheses separated by commas and one or more statements that define the body of the function.
// A function by itself doesn't do anything. In order to run a function, you have to invoke it. (Example 1)

// Difference between argument and parameter - an argument is a value that we pass into the function when we invoke it.
// A parameter is a variable that we list as a part of our function declaration.

// When your function is returning a value (using the 'return' keyword), you should assign it to a variable. (Example 2)

// The Arguments Object (Example 3)

// Example 1
function greetings(name) {
  console.log(`Hello, ${name}!`);
}
greetings("John"); // Hello, John!

// Example 2
function sum(num1, num2) {
  return num1 + num2;
}
let result = sum(2, 3);
console.log(result); // 5

// Example 3
function printAll() {
  for (let i = 0; i < arguments.length; i++) {
    console.log(arguments[i]);
  }
}
printAll(1, 3, 7, 21); // 1, 3, 7, 21

// Function Scope
// It is important that we understand the lifetime of a variables within a function.
// In this example (Example 4) we have a function 'greeting()' that simply declares a variable 'message' and assigns the value 'hello' to it.
// We invoke the 'greeting()' function first. When we try to access the 'message' variable, we get a refference error. This happen because
// 'message' has a function scope and it cannot be accessed outside its scope.
// Whenever a function cannot find a variable, it looks to its parent function. JavaScript will keep looking at a parent and its
// parent for deeply nested functions until it either finds a variable or throw a refference error. (Example 5)

// Example 4
function sayHello() {
  let message = "Hello!";
}
sayHello();
console.log(message); // Refference Error

// Example 5
function saySomething() {
  let words = "Hello!";
  let sayHi = function hi() {
    console.log(words);
  };

  sayHi(); // Hello!
}
saySomething();

// Block Scope
// When we say 'block', we are reffering to any code that it is closed inside a curly braces. With block scope, we are talking
// about the lifetime of a variables within the curly braces of an 'if' statement, a 'for' or 'while' loop, or any other set of curly braces
// other than a function.
// !!IMPORTANT - Variables declared with the 'var' keyword or within function declarations DO NOT have block scope. (Example 6) (Example 7)

// Example 6
let emailInvitation = "You have been invited to an event!";
if (emailInvitation === "You have been invited to an event!") {
  let count = 100;
}
console.log(count); // Error

// Example 7
let emailReminder = "Your meeting starts in less than 15 minutes!";
if (emailReminder === "Your meeting starts in less than 15 minutes!") {
  let emailReminder = "Inside 'if' block!";
  console.log(emailReminder); // "Inside 'if' block!"
}
console.log(emailReminder); // "Your meeting starts in less than 15 minutes!"

// The IIFE pattern lets us group our code and have it work in isolation, indepentend ot any other code. IIFE stands for 'Immediately Invoked
// IIFE stands for 'Immediately Invoked Function Expression'. // Function Expresison -> define a function and assign it to a variable.
// Immediately Invoked -> invokes the function immediately where it's defined. IIFE functions does not have a name and it's completely
// inside parentheses. (Example 8)

// Example 8
(function () {
  console.log("Hello!");
})();

// Soon to be added: 
// Differences between function declarations, function expressions and arrow functions + examples
