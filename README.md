# Learn JS - Description

The content of this repository is created for everybody that wants to learn JavaScript from scratch or just improve their programming skills. If you like how the lessons are structured or you just find the content useful, feel free to request any specific JS content, with more in-depth explanations.

Reach me at any time: g.mladenov.dev@gmail.com
